#include <exception>
#include <boost/log/trivial.hpp>
#include <R_ext/Print.h>
#include <R_ext/Rdynload.h>
#include "boost_log_R/sink_backend.hpp"
#include "dll_visibility.h"
#include "log/trivial.hpp"

extern "C" DLL_PUBLIC void R_init_libpackagenamehere(DllInfo*);
void R_init_libpackagenamehere_impl(DllInfo*);

extern "C" DLL_PUBLIC void R_init_libpackagenamehere(DllInfo *dll_info) {
    try {
        R_init_libpackagenamehere_impl(dll_info);
    } catch (std::exception& e) {
        REprintf("packagenamehere initialisation failed with C++ exception: %s\n", e.what());
    } catch (...) {
        REprintf("packagenamehere initialisation failed with unknown C++ exception.\n");
    }
}

void R_init_libpackagenamehere_impl(DllInfo *dll_info) {
    // Initialise libraries here.
    initialise_boost_log_R_sink_backend();

    // Register native routines here.
    /*
    static const R_CallMethodDef callMethods[] = {
        {"faefs", (DL_FUNC) &Rfaefs, 11},
        {nullptr, nullptr, 0}
    };
    R_registerRoutines(
        dll_info,
        nullptr,        // .C
        callMethods,    // .Call
        nullptr,        // .Fortran
        nullptr         // .External
    );
    */

    // Ensure that this dll is not searched for
    // entry points specified by character strings,
    // and that only registered routines in this
    // dll may be called.
    /*
    R_useDynamicSymbols(dll_info, FALSE);
    R_forceSymbols(dll_info, TRUE);
    */
}

