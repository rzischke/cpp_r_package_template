#ifndef PACKAGENAMEHERE_LOG_TRIVIAL_GUARD
#define PACKAGENAMEHERE_LOG_TRIVIAL_GUARD

#include <utility>
#include <boost/log/trivial.hpp>

#ifdef NDEBUG
    namespace packagenamehere {

        class null_logger {
            public:
                template <typename T>
                null_logger& operator<<(const T& t) {
                    return *this;
                }
        };

    }

    #define PACKAGENAMEHERE_LOG_TRIVIAL_TRACE if (false) ::packagenamehere::null_logger()
    #define PACKAGENAMEHERE_LOG_TRIVIAL_DEBUG if (false) ::packagenamehere::null_logger()
#else
    #define PACKAGENAMEHERE_LOG_LOCATION_PRELUDE __FILE__ ":" << __LINE__ << ": "

    #define PACKAGENAMEHERE_LOG_TRIVIAL_TRACE BOOST_LOG_TRIVIAL(trace) << PACKAGENAMEHERE_LOG_LOCATION_PRELUDE
    #define PACKAGENAMEHERE_LOG_TRIVIAL_DEBUG BOOST_LOG_TRIVIAL(debug) << PACKAGENAMEHERE_LOG_LOCATION_PRELUDE
#endif

#define PACKAGENAMEHERE_LOG_TRIVIAL_INFO BOOST_LOG_TRIVIAL(info)
#define PACKAGENAMEHERE_LOG_TRIVIAL_WARNING BOOST_LOG_TRIVIAL(warning)
#define PACKAGENAMEHERE_LOG_TRIVIAL_ERROR BOOST_LOG_TRIVIAL(error)
#define PACKAGENAMEHERE_LOG_TRIVIAL_FATAL BOOST_LOG_TRIVIAL(fatal)

#endif

