cmake_minimum_required( VERSION 3.0 )

add_subdirectory( external )

# All source files need to be compiled together in the one
# cmake exectuable. If this isn't the case, platform dependent
# linker flags must be passed to ensure relevent symbols
# are exposed. This also isn't currently easy to do with cmake.
add_executable( tests
    "dummy/src/dummy.cpp"
    "test_main.cpp"
)
target_link_libraries( tests
    test_boost_test
)
set_target_properties( tests PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON
)

